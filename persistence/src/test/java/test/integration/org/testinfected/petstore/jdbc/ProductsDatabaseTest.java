package test.integration.org.testinfected.petstore.jdbc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;

import java.sql.Connection;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.ProductsDatabase;
import org.testinfected.petstore.product.Attachment;
import org.testinfected.petstore.product.DuplicateProductException;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.product.ProductCatalog;
import org.testinfected.petstore.transaction.QueryUnitOfWork;
import org.testinfected.petstore.transaction.Transactor;

import test.support.org.testinfected.petstore.builders.Builder;
import test.support.org.testinfected.petstore.jdbc.Database;

public class ProductsDatabaseTest {

	private Database database;
    private Connection connection;
    private Transactor transactor;
    private ProductCatalog productCatalog;
    
    @Before
	public void setUp() {
    	database = Database.test();
    	connection = database.connect();
    	transactor = new JDBCTransactor(connection);
    	productCatalog = new ProductsDatabase(connection);
    }
    
    @After
    public void teardown() throws Exception {
    	database.clean();
    	connection.close();
    	connection = null;
    	database = null;
    	transactor = null;
    	productCatalog = null;
    }
	
    @Test
    public void findsProductsByNumber() throws Exception {
    	Product p = aProduct("12345678").build();
    	saveProductToDatabase(p);
        Product found = productCatalog.findByNumber("12345678");
        assertThat("matched product", found.equals(p));
    }
    
	/**
	 * Comprovar que podem trobar un producte buscant per la seva descripció
	 * @author Carlos Lázaro Costa (<i>carlos.lazaro.costa@est.fib.upc.edu</i>)
	 */
    @Test
	public void testFindProductByDescription() throws Exception {
		final String description = "LAB-1234 Description";
		saveProductFrom(aProduct().describedAs(description));
        assertThat("available products",
        		productCatalog.findByKeyword(description),
        		everyItem(hasProductDescription(description)));
	}
	
	/**
	 * Comprovar que els identificadors de producte són unics
	 * @author Carlos Lázaro Costa (<i>carlos.lazaro.costa@est.fib.upc.edu</i>)
	 */
	@Test(expected = DuplicateProductException.class)
	public void testUniqueIdentifiers() throws Exception {
		Product existingProduct = saveProductFrom(aProduct().withNumber("LAB-1234"));
		saveProductToDatabase(existingProduct);
	}
	
    @Test
    public void productNotInDB() {
    	assertThat ("Not in Db", productCatalog.findByNumber("245346") == null);
    }
    
    /** Comprovar que podem trobar un producte buscant el seu nom */
    @Test
    public void testFindProductByName() throws Exception {
    	String name = "Producte de prova 001";
    	saveProductFrom(aProduct().named(name));
    	assertThat("available products", productCatalog.findByKeyword(name), everyItem(hasProductName(name)));
    }
    
    /** Comprovar que podem recuperar la informació completa d'un producte */
    @Test
    public void testGetAllProductInfo() throws Exception {
    	String nom = "Producte de prova 001";
    	String numero = "001";
    	String descripcio = "Descripció de prova 001";
    	String foto = "Foto 001";
    	Product prod = new Product(numero, nom);
    	prod.setDescription(descripcio);
    	prod.attachPhoto(new Attachment(foto));
    	
    	saveProductToDatabase(prod);
    	Product prodFromDB = productCatalog.findByNumber(numero);
    	assertThat("get number", prodFromDB.getNumber(), is(numero));
    	assertThat("get name", prodFromDB.getName(), is(nom));
    	assertThat("get description", prodFromDB.getDescription(), is(descripcio));
    	assertThat("get photo file name", prodFromDB.getPhotoFileName(), is(foto));
    }

	/* Support methods */
	
	private Product saveProductFrom(final Builder<Product> builder) throws Exception {
        return saveProductToDatabase(builder.build());
    }
	
	private Product saveProductToDatabase(Product product) throws Exception {
		return transactor.performQuery(new QueryUnitOfWork<Product>() {
            public Product query() throws Exception {
                productCatalog.add(product);
                return product;
            }
        });
	}
	
    private Matcher<Product> hasProductDescription(final String description) {
        return new FeatureMatcher<Product, String>(equalTo(description), "has product description", "product description") {
            protected String featureValueOf(Product actual) {
                return actual.getDescription();
            }
        };
    }
	
    private Matcher<Product> hasProductName(final String name) {
        return new FeatureMatcher<Product, String>(equalTo(name), "has product name", "product name") {
            protected String featureValueOf(Product actual) {
                return actual.getName();
            }
        };
    }
}
