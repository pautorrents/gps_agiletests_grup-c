package test.integration.org.testinfected.petstore.jdbc;

import static org.hamcrest.MatcherAssert.assertThat;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.OrdersDatabase;
import org.testinfected.petstore.order.CartItem;
import org.testinfected.petstore.order.LineItem;
import org.testinfected.petstore.order.Order;
import org.testinfected.petstore.order.OrderNumber;
import org.testinfected.petstore.transaction.Transactor;

import test.support.org.testinfected.petstore.builders.ItemBuilder;
import test.support.org.testinfected.petstore.builders.OrderBuilder;
import test.support.org.testinfected.petstore.builders.ProductBuilder;
import test.support.org.testinfected.petstore.jdbc.Database;

public class OrdersDatabaseTest {
	Database database;
    Connection connection;
    Transactor transactor;
    OrdersDatabase ordersDatabase;

    @Before public void
    initConnection() throws SQLException {
    	database = Database.test();
    	connection = database.connect();
    	transactor = new JDBCTransactor(connection);
    	ordersDatabase = new OrdersDatabase(connection);
    }
    @After public void
    closeConnection() throws Exception {
    	database.clean();
        connection.close();
    }
    
    @Test public void
    findsOrderByNumber(){
    	OrderBuilder builder = OrderBuilder.anOrder().withNumber("12345678");
    	Order o= builder.build();
        ordersDatabase.record(o);
        OrderNumber num = new OrderNumber ("12345678");
        Order found = ordersDatabase.find(num);
        assertThat("matched order", found.toString().equals(o.toString()));
    }
    
    @Test public void
    DuplicateNotCorromp(){
    	OrderBuilder builder = OrderBuilder.anOrder().withNumber("7654567");
    	Order o= builder.build();
    	ItemBuilder ibuilder = ItemBuilder.anItem().of(ProductBuilder.aProduct());
    	CartItem c = new CartItem(ibuilder.build());
    	LineItem l1 = LineItem.from(c);
    	LineItem l2 = LineItem.from(c);
        o.addLineItem(l1);
        o.addLineItem(l2);
        ordersDatabase.record(o);
        OrderNumber num = new OrderNumber ("7654567");
        Order found = ordersDatabase.find(num);
        assertThat("matched order", found.toString().equals(o.toString()));
    }
    
    
}
