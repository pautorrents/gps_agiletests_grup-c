package test.unit.org.testinfected.petstore.product;

import org.junit.Test;
import org.testinfected.petstore.product.Item;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.anItem;
import static org.hamcrest.MatcherAssert.assertThat;



public class ItemTest {

	@Test
	public void sameNumberSameItem() {
		Item a = anItem().withNumber("1").build();
		Item b = anItem().withNumber("1").build();
		assertThat("Items are equals", a.equals(b));
	}
	
}
