package test.unit.org.testinfected.petstore.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.testinfected.petstore.product.DuplicateItemException;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.product.ItemInventory;
import org.testinfected.petstore.product.ItemNumber;

/**
 * @author Carlos Lázaro Costa (carlos.lazaro.costa@est.fib.upc.edu)
 */
public class FakeItemInventory implements ItemInventory {

	private Map<ItemNumber, Item> items;
	
	public FakeItemInventory() {
		items = new HashMap<ItemNumber, Item>();
	}
	
	@Override
	public List<Item> findByProductNumber(String productNumber) {
		return items.entrySet().stream()
				.filter(e -> e.getKey().getNumber().equals(productNumber))
				.map(e -> e.getValue())
				.collect(Collectors.toList());
	}

	@Override
	public Item find(ItemNumber itemNumber) {
		return items.get(itemNumber);
	}

	@Override
	public void add(Item item) throws DuplicateItemException {
		items.put(new ItemNumber(item.getNumber()), item);
	}
	
	public void addItems(Item... items) {
		try {
			for (Item item : items) add(item);
		} catch (DuplicateItemException ignore) {}
	}

}
